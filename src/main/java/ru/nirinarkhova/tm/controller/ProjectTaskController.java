package ru.nirinarkhova.tm.controller;

import ru.nirinarkhova.tm.api.IProjectTaskController;
import ru.nirinarkhova.tm.api.IProjectTaskService;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showTaskByProjectId() {
        System.out.println("[SHOW TASK BT PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findTaskByProjectId(projectId);
        int index = 1;
        for (final  Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void bindTaskByProject() {
        System.out.println("[BIND TASK BY PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskByProject(taskId, projectId);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK BY PROJECT]");
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(taskId);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        projectTaskService.removeProjectById(projectId);
        System.out.println("[OK]");
    }

}
