package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.IProjectRepository;
import ru.nirinarkhova.tm.api.IProjectTaskService;
import ru.nirinarkhova.tm.api.ITaskRepository;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProject(String taskId, String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        return taskRepository.bindTaskByProject(taskId, projectId);
    }

    @Override
    public Task unbindTaskFromProject(String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        return taskRepository.unbindTaskByProject(taskId);
    }

    @Override
    public Project removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneById(projectId);
    }

}
