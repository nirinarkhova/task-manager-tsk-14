package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project startProjectById(String id);

    Project startProjectByName(String name);

    Project startProjectByIndex(Integer index);

    Project finishProjectById(String id);

    Project finishProjectByName(String name);

    Project finishProjectByIndex(Integer index);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByName(String name, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
