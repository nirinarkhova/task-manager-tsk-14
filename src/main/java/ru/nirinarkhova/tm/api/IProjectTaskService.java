package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String projectId);

     Task bindTaskByProject(String taskId, String projectId);

     Task unbindTaskFromProject(String taskId);

     Project removeProjectById(String projectId);

}
